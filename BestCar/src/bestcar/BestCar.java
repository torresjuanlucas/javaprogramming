/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bestcar;

import java.util.ArrayList;
import java.util.Collections;


/**
 *
 * @author 1795647
 */
public class BestCar {
    
    static ArrayList<Car> garage = new ArrayList<>();
    
    public static void main(String[] args) {
        Car toyota = new Car("Toyota", "Corolla", 120, 8.9, 33.4);
        Car buick = new Car("Buick", "Verano", 180, 6.8, 22.6);
        Car honda = new Car("Honda", "Civic", 160, 7.9, 25.4);
        Car mcLaren = new Car("McLaren", "F1", 340, 2.9, 19.8);
        Car ferrari = new Car("Ferrari", "Enzo", 330, 3.4, 17.4);
        Car lambo = new Car("Lamborghini", "Urraco", 290, 3.6, 18.5);
        Car jeep = new Car("Jeep", "Cherokee", 170, 8.2, 19.5);
        Car bmw = new Car("BMW", "X5", 190, 5.4, 18.4);
        
        garage.add(bmw);
        garage.add(toyota);
        garage.add(buick);
        garage.add(honda);
        garage.add(mcLaren);
        garage.add(ferrari);
        garage.add(lambo);
        garage.add(jeep);
        
        System.out.println(Car.getCount());
        System.out.println(jeep.getUniqueID());
//        System.out.println("===========BEFORE SORTING=======");
//        for (Car c : garage){
//        System.out.printf("%s %s %n",c.make, c.model);
//        }
            garage.add(new Car("BMW", "X7", 210, 4.9, 17.4));
            System.out.println(Car.getCount());
//        Collections.sort(garage);
//        System.out.println("==========AFTER SORTING=======");
//        int i = 0;
//        for (Car temp : garage) {
//            System.out.println("Car & model " + ++i + ": " + temp.make + " "
//                    + temp.model);
//        }
//        
//        Collections.sort(garage, Car.CarComparatorByMaxSpeed);
//        System.out.println("===========AFTER SORTING & CAR COMPARATOR BY SPEED=======");
//        for (Car c : garage){
//        System.out.printf("%s %s %n",c.make, c.model);
//        }
//        
//        Collections.sort(garage, Car.CarComparatorByAccelleration);
//        System.out.println("===========AFTER SORTING & CAR COMPARATOR BY SPEED & ACCELERATION=======");
//        for (Car c : garage){
//        System.out.printf("%s %s %n",c.make, c.model);
//        }
//        
//        Collections.sort(garage, Car.CarComparatorByEconomy);
//        System.out.println("===========AFTER SORTING & CAR COMPARATOR BY SPEED & ACCELERATION & FUEL ECONOMY=======");
//        for (Car c : garage){
//        System.out.printf("%s %s %n",c.make, c.model);
//        }
    }
        
}
