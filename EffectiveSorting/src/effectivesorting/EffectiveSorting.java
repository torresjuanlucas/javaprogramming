/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package effectivesorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 *
 * @author 1795647
 */
public class EffectiveSorting {

    /**
     * @param args the command line arguments
     */
    static ArrayList<Person> people = new ArrayList<>();

    public static void main(String[] args) {
        Person Bob = new Person("Bob", 45);
        Person Frank = new Person("Frank", 22);
        Person Tracy = new Person("Tracy", 33);
        Person Vance = new Person("Vance", 25);
        Person Eric = new Person("Eric", 32);
       // Person Rue = new Person("Rue", 44);
        Person Hank = new Person("Hank", 55);

        people.add(new Person("Rue",44));
       // people.add(Rue);
        people.add(Bob);
        people.add(Frank);
        people.add(Tracy);
        people.add(Vance);
        people.add(Eric);
        people.add(Hank);
        
        Collections.sort(people);
        int i = 0;
        for (Person temp : people) {
            System.out.println("Person " + ++i + ": " + temp.name
                    + ", Age: " + temp.age);
        }

    }

}
