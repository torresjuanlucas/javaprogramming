/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package effectivesorting;


public class Person implements Comparable<Person>{
    String name;
    int age;
    
    public Person(String name, int age) {
        this.age = age;
        this.name = name;
    }
    
    
    @Override
    public int compareTo(Person comparePerson) {
        int compareAge = comparePerson.age;
        
        return this.age - compareAge;
    }
}
