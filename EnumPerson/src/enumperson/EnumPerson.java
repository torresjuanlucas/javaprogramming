/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumperson;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author 1795849
 */
class Person {

    String name;
    Gender gender;
    AgeRange ageRange;

    enum Gender {
        Male, Female, NA
    }

    enum AgeRange {
        Below18, From18to35, From35to65, Over65
    };

    @Override
    public String toString() {
        return String.format("%s is %s is %s", name, gender, ageRange);
    }
}

class ParsingException extends Exception {

    public ParsingException() {
        super();
    }

    public ParsingException(String message) {
        super(message);
    }

    public ParsingException(String message, Throwable cause) {
        super(message, cause);
    }
}

public class EnumPerson {

    public static void main(String[] args) {

        ArrayList<Person> people = new ArrayList<>();

        try {
            Scanner fileInput = new Scanner(new File("input.txt"));
            while (fileInput.hasNextLine()) {
                String line = fileInput.nextLine();
                try {                    
                    String[] data = line.split(";");
                    if (data.length != 3) {
                        throw new ParsingException("Invalid number of fields, 3 expected");
                    }
                    Person p = new Person();
                    p.name = data[0];
                    p.gender = Person.Gender.valueOf(data[1]);
                    p.ageRange = Person.AgeRange.valueOf(data[2]);
                    people.add(p);
                } catch (ParsingException | IllegalArgumentException e) {
                    System.out.println("Error parsing line: " + line);
                    System.out.println("  Reason: " + e.getMessage());
                }
            }
        } catch (IOException e) {
            /// e.printStackTrace();
            System.out.println("File reading error: " + e.getMessage());
        }
        //
        for (Person p : people) {
            System.out.println(p.toString());
        }

    }

}
