/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gemthis;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author 1795849
 */
public class GemThis {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String DATA = "Jerry Boe\n44\nMary White\n77\n";
        Scanner input = new Scanner(DATA);

        String n1 = input.nextLine();
        int n2 = input.nextInt();
        input.nextLine(); // consume the left-over newline character
        
        String n3 = input.nextLine();
        int n4 = input.nextInt();
        input.nextLine(); // consume the left-over newline character

        System.out.printf("n1: %s, n2: %d\n", n1, n2);
        System.out.printf("n3: %s, n4: %d\n", n3, n4);

        /*
        
        Object ooo = new Object();
        Object o2 = new Object();
        System.out.println("ooo is: " + ooo);
        System.out.println("o2  is: " + o2);
        
        try {
            // old way
            ArrayList list = new ArrayList();
            // no control over what you put into the list
            list.add("Jerry was here");
            list.add(new Object());
            list.add(new Scanner(System.in));
            
            for (int i = 0; i < list.size(); i++) {
                Object o = list.get(i);
                String s = (String) o;
                System.out.println(s);
            }
        } catch (ClassCastException e) {
            System.out.println("The old way blowed up: " + e.getMessage() );
        }
        
        {   // new way  using generics
            ArrayList<String> list = new ArrayList<>();
            list.add("Jerry was here");
            // list.add(new Object());
            
            for (int i = 0; i < list.size(); i++) {
                Object o = list.get(i);
                String s = (String) o;
                System.out.println(s);
            } 
        }
        
        */
    }
}
