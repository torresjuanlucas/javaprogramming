
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 1795849
 */
public class NewFileStorage {
    public static String FILE_NAME = "newPeopleListData.txt";

    static void saveNewPeopleList(ArrayList<Person> list) throws IOException {
        //FIXME: use try-with-resources here
  
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(FILE_NAME)));
        for (Person p : list) {
            out.println(p.getName());
            out.println(p.getAge());
            out.println(p.getPostalCode());
        }
        out.close();
    }

    static ArrayList<Person> loadNewPeopleList() throws IOException {
        ArrayList<Person> list = new ArrayList<>();
        try {
            File file = new File(FILE_NAME);
            if(!file.exists()) {
                return list;
            }
            Scanner in = new Scanner(new File(FILE_NAME));
            while (in.hasNextLine()) {
                String name = in.nextLine();
                int age = in.nextInt();
                String postalCode = in.nextLine();
                
                in.nextLine(); //consume the left-over newline character
                Person p = new Person(name, age, postalCode);
                list.add(p);
            }
        } catch (InputMismatchException e) {
            throw new IOException("Error while parcing data file", e);
        } catch (IllegalArgumentException e) {
            throw new IOException("Error while parcing data file", e);
        }
        return list;
    }
}
