import java.util.regex.Pattern;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 1795849
 */
public class Person {
    private String name;
    private int age;
    private String postalCode;
    

    public Person(String name, int age, String postalCode) {
        setName(name);
        setAge(age);
        setPostalCode(postalCode);
    }//end constructor Person

    public String getName() {
        return name;
    }//end method getName

    public final void setName(String name) {
        if (name == null) {
            throw new IllegalArgumentException("name must not be null.");
        }
        if (name.length() < 2 || name.length() > 20) {
            throw new IllegalArgumentException("name must be 2-20 characters long.");
        }
        this.name = name;
    }//end method setName

    public int getAge() {
        return age;
    }//end method getAge

    public final void setAge(int age) {
        this.age = age;
        if (age < 0 || age > 150) {
            throw new IllegalArgumentException("name must be between 0-150.");
        }

    }//end method setAge
    
    public String getPostalCode() {
        return postalCode;
    }//end method getPostalCode

    public final void setPostalCode(String postalCode) {
        if (postalCode == null) {
            throw new IllegalArgumentException("postal code must not be null.");
        }
        if (postalCode.length() < 2 || postalCode.length() > 50) {
            throw new IllegalArgumentException("postal code must have good format.");
        }
        this.postalCode = postalCode;
    }//end method setPostalCode

    @Override
    public String toString() {
        return String.format("%s is %d y/o at %s", name, age, postalCode);
    }
}
