/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpeoplelist;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author 1795693
 */
public class FileStorage {
         
     public FileStorage() {
    }
     
    static void savePeopleToFile(ArrayList<Person> list, File file) throws IOException {
        // FIXME: use try-with-resources here
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file)));
        for (Person p : list) {
            out.print(p.getName() + " ;");
            out.print(p.getAge()+ " ;");
            out.println(p.getPostalCode()+ " ;");
        }
        out.close();
    }

    static ArrayList<Person> loadPeopleFromFile(File file) throws IOException {
        ArrayList<Person> list = new ArrayList<>();
        try {
            //File file = new File(FILE_NAME);
            if (!file.exists()) {
                return list;
            }
            Scanner in = new Scanner(file);
            while (in.hasNextLine()) {
                String line = in.nextLine();
                String[] fields = line.split(";");
                
                if (fields.length == 3) {
                    String name = fields[0].trim(); 
                    int age = 0;
                    
                    if (!"".equals(fields[1].trim())){
                        age = Integer.parseInt(fields[1].trim());
                    }
                    
                    String postalCode = fields[2].trim();

                    Person p = new Person(name, age, postalCode);
                    list.add(p);
                }
            }
            System.out.println(list);
        } catch (InputMismatchException e) {
            throw new IOException("Error while parsing data file", e);
        } catch (IllegalArgumentException e) {
            throw new IOException("Error while parsing data file", e);
        }        
        return list;
    }
}
