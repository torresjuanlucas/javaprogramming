
package newpeoplelist;

import javax.swing.JOptionPane;

/**
 *
 * @author 1795693
 */
public class Person {
    private String name;
    private int age;
    private String postalCode;

    public Person(String name, int age, String postalCode) {
        setName(name);
        setAge(age);
        setPostalCode(postalCode);
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public final void setName(String name) {
        if ((name.length() < 2 || name.length() > 20)) {
            throw new IllegalArgumentException("name must be 2 - 20 characters long");
        }        
        this.name = name;
    }

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public final void setAge(int age) {
        if (age < 1 || age > 150) {
            throw new IllegalArgumentException("Age must be between 1 - 150");
        } 
        this.age = age;
    }

    /**
     * @return the postalCode
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * @param postalCode the postalCode to set
     */
    public final void setPostalCode(String postalCode) {
        if (!postalCode.toUpperCase().matches("[A-Z][0-9][A-Z] ?[0-9][A-Z][0-9]")) {
            throw new IllegalArgumentException("Postal code is not in correct format");
        }
        this.postalCode = postalCode.toUpperCase();
    }
    
    
    @Override
    public String toString() {
      return String.format("%s is %d y/o at Postalcode %s", name ,age, postalCode);
    }

    
}
