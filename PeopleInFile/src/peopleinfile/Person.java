/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peopleinfile;

/**
 *
 * @author 1795849
 */
public class Person {

    private String name;
    private int age;
    // TODO: enum Gender

    public Person(String name, int age) {
        setName(name);
        setAge(age);
    }//end constructor Person

    public String getName() {
        return name;
    }//end method getName

    public final void setName(String name) {
        if (name == null) {
            throw new IllegalArgumentException("name must not be null.");
        }
        if (name.length() < 2 || name.length() > 50) {
            throw new IllegalArgumentException("name must be 2-50 characters long.");
        }
        this.name = name;
    }//end method setName

    public int getAge() {
        return age;
    }//end method getAge

    public final void setAge(int age) {
        this.age = age;
        if (age < 0 || age > 150) {
            throw new IllegalArgumentException("name must be between 0-150.");
        }

    }//end method setAge

    @Override
    public String toString() {
        return String.format("%s is %d y/o", name, age);
    }
}
