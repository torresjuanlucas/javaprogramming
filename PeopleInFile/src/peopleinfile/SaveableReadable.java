/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peopleinfile;

/**
 *
 * @author 1795849
 */
public interface SaveableReadable<T> {

    public String saveToString();

    public T readFromString(String data);

}
