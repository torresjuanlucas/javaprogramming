
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 1795849 juan Lucas torres
 */
public class FileStorageTavel {
     public FileStorageTavel() {
    }
     
    static void savePeopleToFile(ArrayList<Trip> list, File file) throws IOException {
        // FIXME: use try-with-resources here
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file)));
        for (Trip p : list) {
            out.print(p.getDestination() + " ;");
            out.print(p.getName() + " ;");
            out.print(p.getPassport() + " ;");
            out.print(p.getDepDate() + " ;");
            out.print(p.getRetDate() + " ;");
        }
        out.close();
    }

    static ArrayList<Trip> loadPeopleFromFile(File file) throws IOException {
        ArrayList<Trip> list = new ArrayList<>();
        try {
            //File file = new File(FILE_NAME);
            if (!file.exists()) {
                return list;
            }
            Scanner in = new Scanner(file);
            while (in.hasNextLine()) {
                String line = in.nextLine();
                String[] fields = line.split(";");
                
                if (fields.length == 5) {
                    String destination = fields[0].trim(); 
                    String name = fields[0].trim();
                    String passport = fields[0].trim();
                    String depDate = fields[0].trim();
                    String retDate = fields[0].trim();
                    
                    
                    
                    String postalCode = fields[2].trim();

                    Trip p = new Trip(destination, name, passport, depDate, retDate);
                    list.add(p);
                }
            }
            System.out.println(list);
        } catch (InputMismatchException e) {
            throw new IOException("Error while parsing data file", e);
        } catch (IllegalArgumentException e) {
            throw new IOException("Error while parsing data file", e);
        }        
        return list;
    }
}
