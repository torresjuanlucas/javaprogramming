
import java.text.ParseException;
import java.text.SimpleDateFormat;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 1795849 juan Lucas torres
 */
public class Trip {
    private String destination;
    private String name;
    private String passport;
    private String depDate;
    private String retDate;

    public Trip (String destination, String name, String passport, String depDate, String retDate) {
        setDestination(destination);
        setName(name);
        setPassport(passport);
        setDepDate(depDate);
        setRetDate(retDate);
    }
    
        /**
     * @return the destination
     */
    public String getDestination() {
        return destination;
    }
    
    /**
     * @param destination the name to set
     */
    public final void setDestination(String destination) {
        if ((destination.length() < 1 || destination.length() > 50)) {
            throw new IllegalArgumentException("destination must be 1 - 50 characters long");
        }        
        this.destination = destination;
    }
    
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    
    /**
     * @param name the name to set
     */
    public final void setName(String name) {
        if ((name.length() < 1 || name.length() > 50)) {
            throw new IllegalArgumentException("name must be 1 - 50 characters long");
        }        
        this.name = name;
    }
    
    /**
     * @return the postalCode
     */
    public String getPassport() {
        return passport;
    }

    /**
     * @param passport the postalCode to set
     */
    public final void setPassport(String passport) {
        if (!passport.toUpperCase().matches("[A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9]")) {
            throw new IllegalArgumentException("Passport has incorrect format");
        }
        this.passport = passport.toUpperCase();
    }
    
      /**
     * @return the postalCode
     */
    public String getDepDate() {
        return depDate;
    }

    
    public static boolean isValid(String depDate) {
    if (depDate == null || !depDate.matches("\\d{4}-[01]\\d-[0-3]\\d"))
        return false;
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    df.setLenient(false);
    try {
        df.parse(depDate);
        return true;
    } catch (ParseException ex) {
        return false;
    }
}
    /**
     * @param depDate the departure date to set
     */
    public final void setDepDate(String depDate) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        if (!depDate.matches("\\d{4}-[01]\\d-[0-3]\\d")) {
            throw new IllegalArgumentException("depDate has incorrect format");
        }
        
        this.depDate = depDate.toUpperCase();
    }
    
      /**
     * @return the retDate
     */
    public String getRetDate() {
        return retDate;
    }

    /**
     * @param retDate the return date to set
     */
    public final void setRetDate(String retDate) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        if (!retDate.matches("\\d{4}-[01]\\d-[0-3]\\d")) {
            throw new IllegalArgumentException("retDate has incorrect format");
        }
        this.retDate = retDate.toUpperCase();
    }
    
    @Override
    public String toString() {
      return String.format("%s is %d y/o at Postalcode %s", destination, name, passport, depDate, retDate);
    }
    
    
}
