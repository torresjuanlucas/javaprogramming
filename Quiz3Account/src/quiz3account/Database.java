/* Juan Lucas Torres
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz3account;

import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author 1795849
 */
public class Database {

    private final static String HOSTNAME = "localhost:3333";
    private final static String USERNAME = "quiz3Account";
    private final static String DBNAME = "quiz3Account";
    private final static String PASSWORD = "ce2xftEnmG78aNdR";
    
    private Connection conn;

    public Database() throws IOException, SQLException {
        conn = DriverManager.getConnection(
                "jdbc:mysql://" + HOSTNAME + "/" + DBNAME, USERNAME, PASSWORD);
    }
    
public void addTransaction(Transaction t) throws IOException, SQLException {
String sql = "INSERT INTO t (date, deposit, withdrawal) VALUES (?, ?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, t.date);
            stmt.setString(2, t.deposit);
            stmt.setString(3, t.withdrawal);
            

            stmt.executeUpdate();
        }
    }

    public ArrayList<Transaction> getAllTransactions() throws IOException, SQLException {
String sql = "SELECT * FROM transactions";
        ArrayList<Quiz3Account> list = new ArrayList<>();

        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);

            while (result.next()) {
                Transaction t = new Transaction();
                
                t.date = result.getDate("date");
                t.deposit = result.getBigDecimal("deposit");
                t.withdrawal = result.getBigDecimal("withdrawal");

                list.add(Transaction);
            }
        }
        catch (NumberFormatException e){
            throw new SQLException("Error parsing price field, e");
        }
        return list;
    }

    
}
