/* Juan Lucas torres
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz3account;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author 1795849
 */
public class Transaction {

    Date date;
    BigDecimal deposit;
    BigDecimal withdrawal;

   

    public BigDecimal getDepositAmount() {
        return deposit;
    }

    public BigDecimal getWithdrawalAmount() {
        return withdrawal;
    }

    public Date getTransactionDate() {
        return date;
    }

   @Override
    public String toString() {
        return String.format("%s(%s): %s", date, deposit, withdrawal);
    }
    
     
 

    
    

}
