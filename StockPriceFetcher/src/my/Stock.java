/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my;

import java.math.BigDecimal;


/**
 *
 * @author 1795849
 */
public class Stock {
    int id;
    String symbol;
    String name;
    BigDecimal price;

    @Override
    public String toString() {
        return String.format("%s(%s): %s", symbol, name, price);
    }
    
    
}




