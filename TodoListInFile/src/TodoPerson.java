
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 1795849
 */
public class TodoPerson {
    private String task;
    private String dueDate;
    

    public TodoPerson(String task, String dueDate) {
        setTask(task);
        setDueDate(dueDate);
    }//end constructor Person

    public String getTask() {
        return task;
    }//end method getTask

    public final void setTask(String task) {
        if (task == null) {
            throw new IllegalArgumentException("task must not be null.");
        }
        if (task.length() < 1 || task.length() > 100) {
            throw new IllegalArgumentException("task must be between 1 to 100 characters long.");
        }
        this.task = task;
    }//end method setTask

    public String getDueDate() {
        return dueDate;
    }//end method getDate

    public final void setDueDate(String dueDate) {
        if (dueDate == null) {
            throw new IllegalArgumentException("date must not be null.");
        

    }//end method setDate

    @Override
    public String toString() {
        return String("%s is %d y/o", task, dueDate);
    }
}
