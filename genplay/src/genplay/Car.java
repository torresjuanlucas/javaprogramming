/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genplay;

import java.util.Comparator;

/**
 *
 * @author namel
 */
public class Car implements Comparable<Car> {

    String make; // e.g. BMW, Toyota, etc.
    String model; // e.g. X5, Corolla, etc.
    int maxSpeedKmph; // maximum speed in km/h
    double secTo100Kmph; // accelleration time 1-100 km/h in seconds
    double litersPer100km; // economy: liters of gas per 100 km
    public static int counter;
    public Car(String make, String model, int maxSpeedKmph, double secTo100Kmph, double litersPer100km) {
        this.make = make;
        this.model = model;
        this.maxSpeedKmph = maxSpeedKmph;
        this.secTo100Kmph = secTo100Kmph;
        this.litersPer100km = litersPer100km;
        counter++;
    }
    
    
    //counter
    public static int getCount(){       
        return counter;
    }
    
    /* Make class Car implement Comparable interface and make
        it compare cars by make then model alphabetically.
        E.g. Toyota Echo will come before Toyota Matrix */
    
    @Override
    public int compareTo(Car compareCar) {
        return this.model.compareTo(compareCar.model);
    }
    
   
       // CarComparatorByMaxSpeed 
    public static Comparator<Car> CarComparatorByMaxSpeed 
            = new Comparator<Car>(){
                @Override
                public int compare(Car car1, Car car2) {
                    int maxSpeed1 = car1.maxSpeedKmph;
                    int maxSpeed2 = car2.maxSpeedKmph;
                    
                    return maxSpeed2 - maxSpeed1;
                }
            };
    
       // CarComparatorByAccelleration
    public static Comparator<Car> CarComparatorByAccelleration
            = new Comparator<Car>() {
                @Override
                public int compare(Car car1, Car car2) {
                    double accel1 = car1.secTo100Kmph;
                    double accel2 = car2.secTo100Kmph;
                    
                    return Double.compare(accel1, accel2);
                }
            };
    
       // CarComparatorByEconomy
    public static Comparator<Car> CarComparatorByEconomy
            = new Comparator<Car>(){
                @Override
                public int compare(Car car1, Car car2) {
                    double liters1 = car1.litersPer100km;
                    double liters2 = car2.litersPer100km;
                    
                    return Double.compare(liters1, liters2);
                }
            };    
}
