/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genplay;

/**
 *
 * @author namel
 */
public class GenPlay {

    public static void main(String[] args) {
        Stack<Car> stackOfCars = new Stack<>();
        Stack<String> stackOfStrings = new Stack<>();

        System.out.printf("Stack of cars %d tall, stack of strings is %d tall %n",
                stackOfCars.getHeight(), stackOfStrings.getHeight());

        stackOfCars.push(new Car("Honda", "Accord", 170, 8.7, 19.9));
        stackOfCars.push(new Car("McLaren", "F1", 390, 2.9, 13.4));
        stackOfCars.push(new Car("Hyundai", "Elantra", 168, 10.3, 21.1));
        stackOfStrings.push("word");
        stackOfStrings.push("dark drone");
        stackOfStrings.push("ambient");

        System.out.printf("Stack of cars %d tall, stack of strings is %d tall %n",
                stackOfCars.getHeight(), stackOfStrings.getHeight());

        stackOfStrings.pop();
        stackOfCars.pop();

        System.out.printf("Stack of cars %d tall, stack of strings is %d tall %n",
                stackOfCars.getHeight(), stackOfStrings.getHeight());

        for (int i = 0; i < 3; i++) {
            String s = stackOfStrings.pop();
            System.out.println("Popped: " + s);
        }

        stackOfStrings.push("ambient");
        stackOfStrings.pop();
        stackOfStrings.push("ambient");
        stackOfStrings.push("aphex twin");
        stackOfStrings.pop();
        stackOfStrings.pop();
        stackOfStrings.pop(); // returns 0 wont return error
        System.out.printf("Stack of cars %d tall, stack of strings is %d tall %n",
                stackOfCars.getHeight(), stackOfStrings.getHeight());

    }

}
