package genplay;

import java.util.ArrayList;

/**
 *
 * @author namel
 * @param <T>
 */
public class Stack<T> {

    int height;
    ArrayList<T> stack;

    public Stack() {
        stack = new ArrayList<>();
    }

    public int getHeight() {
        height = stack.size();
        return height;
    }

    public void push(T item) {
        stack.add(0, item);
    }

    public T pop() throws IndexOutOfBoundsException {
        try {
            if (!stack.isEmpty()) {
                return stack.remove(0);
            } else {
                return null;
            }
        } catch (IndexOutOfBoundsException e) {
            throw new IndexOutOfBoundsException("Deleted amount exceeds actual amount " + e);
        }
    }
}
